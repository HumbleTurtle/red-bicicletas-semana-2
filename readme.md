# Uso
    git clone https://HumbleTurtle@bitbucket.org/HumbleTurtle/red-bicicletas.git

    npm run devstart
# Rutas

    GET     /bicicletas
            Lista de bicicletas

    GET     /bicicletas/:code/update
            Formulario de edición

    POST    /bicicletas/:code/update
            Datos en formato json posteados al uri

    POST    /bicicletas/:code/delete
            Borrado del elemento codeg

# Endpoints

    GET     /api/bicicletas

    POST    /api/bicicletas/create
            Para postman usar body con formato x-www-form-urlencoded

            Campos esperados
                code : code bicicleta
                color  : color de la bicicleta
                modelo : modelo de la bicicleta
                lng : longitud de la coordenada
                lat : latitud de la coordenada


    POST    /api/bicicletas/:id/update
            Enviar datos como params en postman

            Campos esperados
                code : code bicicleta

    POST    /api/bicicletas/:id/delete
            Enviar datos como params en postman

            Campos esperados
                code : code bicicleta
