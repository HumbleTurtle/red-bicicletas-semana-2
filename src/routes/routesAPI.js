var express = require('express');
var path = require('path');

var router = express.Router();

// Subrutas
var bicicletasAPIRouter = require('./api/bicicletas');

router.use("/bicicletas", bicicletasAPIRouter );

module.exports = router;