var mongoose = require('mongoose');
var Reserva = require('./reserva');

var Schema = mongoose.Schema;

const validateEmail = function(email) {
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email)
}

var usuarioSchema = new Schema({

    nombre: {
        type: String, 
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },

    email:{
        type: String,
        trim: true,
        required: true,
        lowercase: true,
        validate: [validateEmail, 'Pr favor, ingrese un email válido'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },

    password: {
        type: String,
        required: [true, 'El password es obligatorio']
    },

    passwordResetToken: String,
    passwordResetTokenExpires: Date
});

usuarioSchema.methods.reservar = async function(biciId, desde, hasta, cb ) {
    var reserva = new Reserva( { usuario: this._id, bicicleta: biciId, desde, hasta} );

    if(cb)
        await reserva.save(cb);
    else
        await reserva.save();
    
    return reserva;
}

module.exports = mongoose.model('Usuario', usuarioSchema);