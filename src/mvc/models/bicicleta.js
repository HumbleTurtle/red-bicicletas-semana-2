const { Schema } = require("mongoose");

var mongoose = require('mongoose');

var bicicletaSchema = new Schema( {
    code: Number,
    color: String,
    modelo: String,

    ubicacion: {
        type: [Number], index: { type: '2dsphere', sparse: true }
    }
});

bicicletaSchema.methods.toString = function () {
    return 'code: '+ this.code + " | color: "+ this.color;
}

bicicletaSchema.statics.createInstance = function( code, color, modelo, ubicacion ) {
    return new this({
        code,
        color,
        modelo,
        ubicacion
    })
}

bicicletaSchema.statics.allBicis = function (callback) {
    if ( callback )
        return this.find({}, callback);
    return this.find( {} );
}

bicicletaSchema.statics.add = function (aBici, callback) {
    
    if ( callback )
        return this.create(aBici, callback);

    return new Promise( async(resolve, reject) => {
        var bici = await this.create(aBici);
        await bici.save();

        resolve(bici);
    });

}

bicicletaSchema.statics.findByCode = function (aCode, callback) {
    if (callback)
        return this.findOne({code: aCode}, callback)

    return this.findOne({code: aCode})
}

bicicletaSchema.statics.removeByCode = function (aCode, callback) {
    if (callback)
        return this.deleteOne( {code: aCode}, callback);
    
    return this.deleteOne( {code: aCode} );
}

module.exports = mongoose.model('Bicicleta', bicicletaSchema);